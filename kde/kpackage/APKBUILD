# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpackage
pkgver=5.55.0
pkgrel=0
pkgdesc="Framework that lets applications manage user installable packages of non-binary assets"
arch="all"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1"
depends_dev="kcoreaddons-dev karchive-dev ki18n-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev kdoctools-dev doxygen"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Fails due to requiring installed Plasma, which causes a circular dependency

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="28656d175fc20ebab4d585737df20860c485b3f22e08c7b44054aea8dce50720d236b496c103b21dbdf773e41652022ff0247a3946dae533f225bbc8f0ff8ddf  kpackage-5.55.0.tar.xz"
