# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kactivitymanagerd
pkgver=5.15.2
pkgrel=0
pkgdesc="System service to manage user's activities and track the usage patterns"
arch="all"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0"
depends_dev="qt5-qtbase-dev kio-dev kdbusaddons-dev ki18n-dev kconfig-dev kcoreaddons-dev kwindowsystem-dev kglobalaccel-dev kxmlgui-dev kconfigwidgets-dev kauth-dev kcodecs-dev kwidgetsaddons-dev kservice-dev kbookmarks-dev kcompletion-dev kitemviews-dev kjobwidgets-dev solid-dev"
makedepends="$depends_dev extra-cmake-modules boost-dev"
source="https://download.kde.org/stable/plasma/$pkgver/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-lang"
builddir="$srcdir/$pkgname-$pkgver/build"

prepare() {
	mkdir "$builddir"
}

build() {
	cd "$builddir"
	cmake .. \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}
sha512sums="b52ac4f8dade81a89f37143dd0d28e53a8165675d9ceb0829c44f416bd61ffc086560163a51660aefe87117431433c07757d7bac43d1b035fd9a4972c1e748ad  kactivitymanagerd-5.15.2.tar.xz"
